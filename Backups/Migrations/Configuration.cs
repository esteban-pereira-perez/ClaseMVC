using ClaseMVC.Models;
using WebMatrix.WebData;
using System.Web.Security;
namespace ClaseMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClaseMVC.Models.PeliculaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;


        }

        protected override void Seed(ClaseMVC.Models.PeliculaDBContext context)
        {
            Random rnd = new Random();
            for (int index = 0; index < 200; index++)
            {
                context.peliculas.AddOrUpdate(i => i.Titulo,
                    new Pelicula
                        {
                            Titulo = "Jurassic Park " + index.ToString(),
                            Fecha = DateTime.Parse("1989-1-11"),
                            ActorPrincipal = "T-Rex",
                            Precio = 7,
                            ruta = "/Content/imagenes/IMG0" + rnd.Next(1, 9).ToString() + ".jpg"
                        });
            }

            CrearUsuarios();
        }

        private void CrearUsuarios()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection","UserProfile","UserId","UserName", autoCreateTables:true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider) Membership.Provider;

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }
            if (membership.GetUser("epereira", false) == null)
            {
                membership.CreateUserAndAccount("epereira", "password");
            }
            if (!roles.GetRolesForUser("epereira").Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { "epereira" }, new[] { "admin" });
            }
        }
    }
}
