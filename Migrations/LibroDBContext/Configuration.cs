using ClaseMVC.Models;

namespace ClaseMVC.Migrations.LibroDBContext
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClaseMVC.Models.LibroDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"Migrations\LibroDBContext";
        }

        protected override void Seed(ClaseMVC.Models.LibroDBContext context)
        {
            #region Libros
            for (int index = 0; index < 200; index++)
            {
                context.Libros.AddOrUpdate(i => i.Titulo,
                    new Libro
                    {
                        Titulo = "Harry Potter " + index.ToString(),
                        Genero = "Ficcion",
                        Precio = 7,
                        Autor = "JK Alguien"
                    }
               );
            }
            #endregion
        }
    }
}
