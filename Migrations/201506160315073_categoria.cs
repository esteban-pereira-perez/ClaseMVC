namespace ClaseMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class categoria : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Peliculas", "Categoria", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Peliculas", "Categoria");
        }
    }
}
