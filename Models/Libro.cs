﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ClaseMVC.Models
{
    public class Libro
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "* {0} Requerido")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "El {0} debe ser de al menos {2} carácteres")]
        public string Titulo { get; set; }

        [Required]
        public string Autor { get; set; }

        [Required]
        public string Genero { get; set; }

        public decimal Precio { get; set; }
    }

    public class LibroDBContext : DbContext
    {
        public LibroDBContext()
            : base("LibrosConnection")
        {
        }

        public DbSet<Libro> Libros { get; set; }

        protected override void OnModelCreating(DbModelBuilder dbModelBuilder)
        {
            dbModelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}