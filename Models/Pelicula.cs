﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClaseMVC.Models
{
    public enum Categoria
    {
        [Display(Name = "Ciencia Ficcion")]
        Ficcion,
        Drama,
        [Display(Name = "comedia")]
        Comedia
    }

    public class Pelicula
    {
        public int id { get; set; }
        [Required(ErrorMessage="*")]
        public string Titulo { get; set; }
        [Required(ErrorMessage = "*")]
        public string ActorPrincipal { get; set; }
               
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        public decimal Precio { get; set; }

        public Categoria? Categoria { get; set; }
        public string ruta { get; set; }
    }

    public class PeliculaDBContext : DbContext
    {
        public PeliculaDBContext()
            : base("PeliculaConnection")
        {
        }

        public DbSet<Pelicula> peliculas { get; set; }
    }
}