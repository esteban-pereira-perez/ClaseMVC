﻿using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ClaseMVC.Models;
using System.Web;
using PagedList;

namespace ClaseMVC.Controllers
{
    public class PeliculaController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        //
        // GET: /Pelicula/

        public ActionResult Index(int pagina = 1)
        {
            return View(db.peliculas.ToList().ToPagedList(pagina,14));
        }

        //
        // GET: /Pelicula/Details/5

        public ActionResult Details(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        public ActionResult llamarAPI()
        {
            return View();
        }

        //
        // GET: /Pelicula/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Pelicula/Create

        [HttpPost]
        public ActionResult Create(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/imagenes"), fileName);
                    file.SaveAs(path);
                    //path = Url.Content(Path.Combine("~/Content/imagenes", fileName));
                    path = "/content/imagenes/" + fileName;
                    pelicula.ruta = path;
                }
                db.peliculas.Add(pelicula);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(pelicula);
            //if (ModelState.IsValid)
            //{
            //    db.peliculas.Add(pelicula);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            //return View(pelicula);
        }

        //
        // GET: /Pelicula/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Edit/5

        [HttpPost]
        public ActionResult Edit(Pelicula pelicula, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/imagenes"), fileName);
                file.SaveAs(path);
                //path = Url.Content(Path.Combine("~/Content/imagenes", fileName));
                path = "/content/imagenes/" + fileName;
                pelicula.ruta = path;
            }
            db.Entry(pelicula).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

            //if (ModelState.IsValid)
            //{
            //    db.Entry(pelicula).State = EntityState.Modified;
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            //return View(pelicula);
        }

        //
        // GET: /Pelicula/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        //
        // POST: /Pelicula/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Pelicula pelicula = db.peliculas.Find(id);
            db.peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}