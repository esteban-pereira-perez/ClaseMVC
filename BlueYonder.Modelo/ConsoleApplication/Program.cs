﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication.Models;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            BlueYonderContext db = new BlueYonderContext();
            foreach (Location loc in db.Locations.ToList())
            {
                Console.WriteLine(loc.Country + " " + loc.CountryCode + " Vuelos:");
                foreach (Flight vuelo in loc.Flights.ToList())
                {
                    Console.WriteLine(vuelo.FlightNumber);
                }
            }
            //var lugares = db.Database.ExecuteSqlCommand("GetLocations");
            var query = db.Database.SqlQuery<Location>("select * from locations").ToList();

            var idParam = new SqlParameter {ParameterName = "id", Value = 1};

            var results = db.Database.SqlQuery<Location>("GetLocationsByID @id", idParam);
            
            Console.WriteLine("-----------------");
            foreach (Location l in results)
            {
                Console.WriteLine(l.Country + " " + l.CountryCode + " Vuelos:");
                foreach (Flight vuelo in l.Flights.ToList())
                {
                    Console.WriteLine(vuelo.FlightNumber);
                }
            }

            Console.ReadLine();
        }
    }
}
