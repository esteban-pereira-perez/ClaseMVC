using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class FlightSchedule
    {
        public FlightSchedule()
        {
            this.Trips = new List<Trip>();
        }

        public int FlightScheduleId { get; set; }
        public System.DateTime Departure { get; set; }
        public Nullable<System.DateTime> ActualDeparture { get; set; }
        public System.TimeSpan Duration { get; set; }
        public int FlightId { get; set; }
        public virtual Flight Flight { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
