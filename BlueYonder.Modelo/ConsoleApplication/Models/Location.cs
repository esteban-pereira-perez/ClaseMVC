using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class Location
    {
        public Location()
        {
            this.Flights = new List<Flight>();
            this.Flights1 = new List<Flight>();
        }

        public int LocationId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string ThumbnailImageFile { get; set; }
        public string TimeZoneId { get; set; }
        public virtual ICollection<Flight> Flights { get; set; }
        public virtual ICollection<Flight> Flights1 { get; set; }
    }
}
