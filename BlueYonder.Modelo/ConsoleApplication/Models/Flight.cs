using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class Flight
    {
        public Flight()
        {
            this.FlightSchedules = new List<FlightSchedule>();
        }

        public int FlightId { get; set; }
        public string FlightNumber { get; set; }
        public int FrequentFlyerMiles { get; set; }
        public Nullable<int> Source_LocationId { get; set; }
        public Nullable<int> Destination_LocationId { get; set; }
        public virtual Location Location { get; set; }
        public virtual Location Location1 { get; set; }
        public virtual ICollection<FlightSchedule> FlightSchedules { get; set; }
    }
}
