using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class Reservation
    {
        public int ReservationId { get; set; }
        public int TravelerId { get; set; }
        public System.DateTime ReservationDate { get; set; }
        public string ConfirmationCode { get; set; }
        public int DepartFlightScheduleID { get; set; }
        public Nullable<int> ReturnFlightScheduleID { get; set; }
        public virtual Trip Trip { get; set; }
        public virtual Trip Trip1 { get; set; }
    }
}
