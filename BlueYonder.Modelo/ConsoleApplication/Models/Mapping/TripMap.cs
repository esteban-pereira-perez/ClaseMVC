using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class TripMap : EntityTypeConfiguration<Trip>
    {
        public TripMap()
        {
            // Primary Key
            this.HasKey(t => t.TripId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Trips");
            this.Property(t => t.TripId).HasColumnName("TripId");
            this.Property(t => t.FlightScheduleID).HasColumnName("FlightScheduleID");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Class).HasColumnName("Class");
            this.Property(t => t.ThumbnailImage).HasColumnName("ThumbnailImage");

            // Relationships
            this.HasRequired(t => t.FlightSchedule)
                .WithMany(t => t.Trips)
                .HasForeignKey(d => d.FlightScheduleID);

        }
    }
}
