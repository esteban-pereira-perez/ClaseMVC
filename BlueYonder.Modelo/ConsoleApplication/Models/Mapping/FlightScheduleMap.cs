using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class FlightScheduleMap : EntityTypeConfiguration<FlightSchedule>
    {
        public FlightScheduleMap()
        {
            // Primary Key
            this.HasKey(t => t.FlightScheduleId);

            // Properties
            // Table & Column Mappings
            this.ToTable("FlightSchedules");
            this.Property(t => t.FlightScheduleId).HasColumnName("FlightScheduleId");
            this.Property(t => t.Departure).HasColumnName("Departure");
            this.Property(t => t.ActualDeparture).HasColumnName("ActualDeparture");
            this.Property(t => t.Duration).HasColumnName("Duration");
            this.Property(t => t.FlightId).HasColumnName("FlightId");

            // Relationships
            this.HasRequired(t => t.Flight)
                .WithMany(t => t.FlightSchedules)
                .HasForeignKey(d => d.FlightId);

        }
    }
}
