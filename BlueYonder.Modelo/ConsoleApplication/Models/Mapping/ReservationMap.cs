using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class ReservationMap : EntityTypeConfiguration<Reservation>
    {
        public ReservationMap()
        {
            // Primary Key
            this.HasKey(t => t.ReservationId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Reservations");
            this.Property(t => t.ReservationId).HasColumnName("ReservationId");
            this.Property(t => t.TravelerId).HasColumnName("TravelerId");
            this.Property(t => t.ReservationDate).HasColumnName("ReservationDate");
            this.Property(t => t.ConfirmationCode).HasColumnName("ConfirmationCode");
            this.Property(t => t.DepartFlightScheduleID).HasColumnName("DepartFlightScheduleID");
            this.Property(t => t.ReturnFlightScheduleID).HasColumnName("ReturnFlightScheduleID");

            // Relationships
            this.HasRequired(t => t.Trip)
                .WithMany(t => t.Reservations)
                .HasForeignKey(d => d.DepartFlightScheduleID);
            this.HasOptional(t => t.Trip1)
                .WithMany(t => t.Reservations1)
                .HasForeignKey(d => d.ReturnFlightScheduleID);

        }
    }
}
