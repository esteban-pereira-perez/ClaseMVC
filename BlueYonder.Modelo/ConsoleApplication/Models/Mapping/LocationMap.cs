using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class LocationMap : EntityTypeConfiguration<Location>
    {
        public LocationMap()
        {
            // Primary Key
            this.HasKey(t => t.LocationId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Locations");
            this.Property(t => t.LocationId).HasColumnName("LocationId");
            this.Property(t => t.Country).HasColumnName("Country");
            this.Property(t => t.State).HasColumnName("State");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.CountryCode).HasColumnName("CountryCode");
            this.Property(t => t.ThumbnailImageFile).HasColumnName("ThumbnailImageFile");
            this.Property(t => t.TimeZoneId).HasColumnName("TimeZoneId");
        }
    }
}
