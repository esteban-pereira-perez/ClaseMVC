using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class FlightMap : EntityTypeConfiguration<Flight>
    {
        public FlightMap()
        {
            // Primary Key
            this.HasKey(t => t.FlightId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Flights");
            this.Property(t => t.FlightId).HasColumnName("FlightId");
            this.Property(t => t.FlightNumber).HasColumnName("FlightNumber");
            this.Property(t => t.FrequentFlyerMiles).HasColumnName("FrequentFlyerMiles");
            this.Property(t => t.Source_LocationId).HasColumnName("Source_LocationId");
            this.Property(t => t.Destination_LocationId).HasColumnName("Destination_LocationId");

            // Relationships
            this.HasOptional(t => t.Location)
                .WithMany(t => t.Flights)
                .HasForeignKey(d => d.Destination_LocationId);
            this.HasOptional(t => t.Location1)
                .WithMany(t => t.Flights1)
                .HasForeignKey(d => d.Source_LocationId);

        }
    }
}
