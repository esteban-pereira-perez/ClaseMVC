using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;

namespace ConsoleApplication.Models.Mapping
{
    public class TravelerMap : EntityTypeConfiguration<Traveler>
    {
        public TravelerMap()
        {
            // Primary Key
            this.HasKey(t => t.TravelerId);

            // Properties
            // Table & Column Mappings
            this.ToTable("Travelers");
            this.Property(t => t.TravelerId).HasColumnName("TravelerId");
            this.Property(t => t.TravelerUserIdentity).HasColumnName("TravelerUserIdentity");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.MobilePhone).HasColumnName("MobilePhone");
            this.Property(t => t.HomeAddress).HasColumnName("HomeAddress");
            this.Property(t => t.Passport).HasColumnName("Passport");
        }
    }
}
