using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class Trip
    {
        public Trip()
        {
            this.Reservations = new List<Reservation>();
            this.Reservations1 = new List<Reservation>();
        }

        public int TripId { get; set; }
        public int FlightScheduleID { get; set; }
        public int Status { get; set; }
        public int Class { get; set; }
        public string ThumbnailImage { get; set; }
        public virtual FlightSchedule FlightSchedule { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
        public virtual ICollection<Reservation> Reservations1 { get; set; }
    }
}
