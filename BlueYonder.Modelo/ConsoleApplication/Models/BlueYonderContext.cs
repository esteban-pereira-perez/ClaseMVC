using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using ConsoleApplication.Models.Mapping;

namespace ConsoleApplication.Models
{
    public partial class BlueYonderContext : DbContext
    {
        static BlueYonderContext()
        {
            Database.SetInitializer<BlueYonderContext>(null);
        }

        public BlueYonderContext()
            : base("Name=BlueYonderContext")
        {
        }

        public DbSet<Flight> Flights { get; set; }
        public DbSet<FlightSchedule> FlightSchedules { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Traveler> Travelers { get; set; }
        public DbSet<Trip> Trips { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new FlightMap());
            modelBuilder.Configurations.Add(new FlightScheduleMap());
            modelBuilder.Configurations.Add(new LocationMap());
            modelBuilder.Configurations.Add(new ReservationMap());
            modelBuilder.Configurations.Add(new TravelerMap());
            modelBuilder.Configurations.Add(new TripMap());
        }
    }
}
