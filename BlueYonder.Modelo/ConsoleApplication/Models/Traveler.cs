using System;
using System.Collections.Generic;

namespace ConsoleApplication.Models
{
    public partial class Traveler
    {
        public int TravelerId { get; set; }
        public string TravelerUserIdentity { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobilePhone { get; set; }
        public string HomeAddress { get; set; }
        public string Passport { get; set; }
    }
}
