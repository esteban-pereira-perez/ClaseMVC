using System;
using System.Collections.Generic;

namespace BlueYonder.WebAPI.Models
{
    public partial class Pelicula
    {
        public int id { get; set; }
        public string Titulo { get; set; }
        public string ActorPrincipal { get; set; }
        public System.DateTime Fecha { get; set; }
        public decimal Precio { get; set; }
        public Nullable<int> Categoria { get; set; }
        public string ruta { get; set; }
    }
}
