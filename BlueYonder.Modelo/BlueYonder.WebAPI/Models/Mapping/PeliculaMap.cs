using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BlueYonder.WebAPI.Models.Mapping
{
    public class PeliculaMap : EntityTypeConfiguration<Pelicula>
    {
        public PeliculaMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.Titulo)
                .IsRequired();

            this.Property(t => t.ActorPrincipal)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Peliculas");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.Titulo).HasColumnName("Titulo");
            this.Property(t => t.ActorPrincipal).HasColumnName("ActorPrincipal");
            this.Property(t => t.Fecha).HasColumnName("Fecha");
            this.Property(t => t.Precio).HasColumnName("Precio");
            this.Property(t => t.Categoria).HasColumnName("Categoria");
            this.Property(t => t.ruta).HasColumnName("ruta");
        }
    }
}
