﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BlueYonder.WebAPI.Models;

namespace BlueYonder.WebAPI.Controllers
{
    public class PeliculaController : ApiController
    {
        private MoviesContext db = new MoviesContext();

        // GET api/Pelicula
        public IEnumerable<Pelicula> GetPeliculas()
        {
            return db.Peliculas.AsEnumerable();
        }

        // GET api/Pelicula/5
        public Pelicula GetPelicula(int id)
        {
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return pelicula;
        }

        // PUT api/Pelicula/5
        public HttpResponseMessage PutPelicula(int id, Pelicula pelicula)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != pelicula.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(pelicula).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Pelicula
        public HttpResponseMessage PostPelicula(Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(pelicula);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, pelicula);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = pelicula.id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Pelicula/5
        public HttpResponseMessage DeletePelicula(int id)
        {
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Peliculas.Remove(pelicula);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, pelicula);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}